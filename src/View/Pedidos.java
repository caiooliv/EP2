
package View;

import Controller.ControlePedidos;
import Model.Cliente;
import Model.Pedido;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class Pedidos extends javax.swing.JFrame {

   
   
    
    private ControlePedidos controlePedidos;
    public Pedidos() {
        initComponents();
            
        jTrocoLabel.setVisible(false);
        jTrocoTextField.setVisible(false);
        jValorRecebidoLabel.setVisible(false);
        jValorRecebidoTextField.setVisible(false);
        
    }

    private void limpaTela(){
         
        jNomeTextField.setText(null);
        jDataTextField.setText(null);
        jObservacaoTextPane.setText(null);
        DefaultListModel listModel = (DefaultListModel) jpedidoList.getModel();
        listModel.removeAllElements();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPedidosPanel1 = new javax.swing.JPanel();
        itensLabel = new javax.swing.JLabel();
        jCancelarButton = new javax.swing.JButton();
        jObsLabel = new javax.swing.JLabel();
        jAdicionarButton = new javax.swing.JButton();
        jPedidoLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jpedidoList = new javax.swing.JList<>();
        jConfirmarButton = new javax.swing.JButton();
        jRetirarButton = new javax.swing.JButton();
        jNomeClienteLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jItensList1 = new javax.swing.JList<>();
        jDataTextField = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        jObservacaoTextPane = new javax.swing.JTextPane();
        jNomeTextField = new javax.swing.JTextField();
        jValorRecebidoPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jClientesTable = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPedidosTextPane = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jValorTotalLabel = new javax.swing.JLabel();
        jPagamentoComboBox = new javax.swing.JComboBox<>();
        jValorRecebidoLabel = new javax.swing.JLabel();
        jTrocoTextField = new javax.swing.JTextField();
        jTrocoLabel = new javax.swing.JLabel();
        jValorRecebidoTextField = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jConfirmaPagamentoButton = new javax.swing.JButton();
        jEstoquePanel = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jEstoqueTable = new javax.swing.JTable();
        jAddEstoqueButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        itensLabel.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        itensLabel.setText("Itens");
        itensLabel.setToolTipText("");

        jCancelarButton.setText("Cancela Pedido");
        jCancelarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCancelarButtonActionPerformed(evt);
            }
        });

        jObsLabel.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        jObsLabel.setText("Observações");

        jAdicionarButton.setText("Adicionar");
        jAdicionarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jAdicionarButtonActionPerformed(evt);
            }
        });

        jPedidoLabel.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        jPedidoLabel.setText("Pedido");

        DefaultListModel model = new DefaultListModel();
        jpedidoList.setModel(model);
        jScrollPane2.setViewportView(jpedidoList);

        jConfirmarButton.setText("Confirma Pedido");
        jConfirmarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jConfirmarButtonActionPerformed(evt);
            }
        });

        jRetirarButton.setText("Retirar");
        jRetirarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRetirarButtonActionPerformed(evt);
            }
        });

        jNomeClienteLabel.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jNomeClienteLabel.setText("Nome do Cliente :");

        jLabel10.setText("Data :");

        DefaultListModel modelList = new DefaultListModel();

        modelList.addElement("pedido1");
        modelList.addElement("pedido2");
        modelList.addElement("pedido3");
        modelList.addElement("pedido4");
        modelList.addElement("pedido5");
        jItensList1.setModel(modelList);
        jScrollPane4.setViewportView(jItensList1);

        jDataTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jDataTextFieldActionPerformed(evt);
            }
        });

        jScrollPane8.setViewportView(jObservacaoTextPane);

        jNomeTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jNomeTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPedidosPanel1Layout = new javax.swing.GroupLayout(jPedidosPanel1);
        jPedidosPanel1.setLayout(jPedidosPanel1Layout);
        jPedidosPanel1Layout.setHorizontalGroup(
            jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                        .addComponent(jNomeClienteLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                        .addComponent(jNomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                        .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jAdicionarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jRetirarButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(itensLabel))
                        .addGap(30, 30, 30)
                        .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPedidoLabel))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jDataTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jCancelarButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jConfirmarButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jObsLabel))
                .addGap(18, 18, 18))
        );
        jPedidosPanel1Layout.setVerticalGroup(
            jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jNomeClienteLabel)
                    .addComponent(jLabel10)
                    .addComponent(jDataTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jNomeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jObsLabel)
                    .addComponent(itensLabel)
                    .addComponent(jPedidoLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jAdicionarButton)
                        .addGap(35, 35, 35)
                        .addComponent(jRetirarButton)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPedidosPanel1Layout.createSequentialGroup()
                        .addGroup(jPedidosPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPedidosPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jConfirmarButton)
                                .addGap(28, 28, 28)
                                .addComponent(jCancelarButton)))
                        .addContainerGap(24, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Pedidos", jPedidosPanel1);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        jLabel4.setText("Lista de Clintes");

        jClientesTable.setModel(new javax.swing.table.DefaultTableModel(
            null,
            new String [] {
                "Nome Cliente"
            }
        ) {

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        });
        jScrollPane5.setViewportView(jClientesTable);
        if (jClientesTable.getColumnModel().getColumnCount() > 0) {
            jClientesTable.getColumnModel().getColumn(0).setResizable(false);
        }

        jPedidosTextPane.setEditable(false);
        jScrollPane6.setViewportView(jPedidosTextPane);

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        jLabel5.setText("Pedido");

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 36)); // NOI18N
        jLabel1.setText("Total");

        jValorTotalLabel.setFont(new java.awt.Font("Ubuntu", 0, 48)); // NOI18N
        jValorTotalLabel.setText("R$ 00.00");

        jPagamentoComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecione um item", "Dinheiro", "Cartão de crédito" }));
        jPagamentoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPagamentoComboBoxActionPerformed(evt);
            }
        });

        jValorRecebidoLabel.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jValorRecebidoLabel.setText("Valor recebido :");

        jTrocoTextField.setEditable(false);
        jTrocoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTrocoTextFieldActionPerformed(evt);
            }
        });

        jTrocoLabel.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jTrocoLabel.setText("Troco :");

        jValorRecebidoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jValorRecebidoTextFieldActionPerformed(evt);
            }
        });

        jButton1.setText("Cancela Pagamento");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jConfirmaPagamentoButton.setText("Confirma Pagamento");

        javax.swing.GroupLayout jValorRecebidoPanelLayout = new javax.swing.GroupLayout(jValorRecebidoPanel);
        jValorRecebidoPanel.setLayout(jValorRecebidoPanelLayout);
        jValorRecebidoPanelLayout.setHorizontalGroup(
            jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                        .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                                .addGap(179, 179, 179)
                                .addComponent(jLabel1))
                            .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                                .addGap(134, 134, 134)
                                .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPagamentoComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jValorTotalLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jValorRecebidoPanelLayout.createSequentialGroup()
                                            .addComponent(jTrocoLabel)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jTrocoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jValorRecebidoPanelLayout.createSequentialGroup()
                                            .addComponent(jValorRecebidoLabel)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jValorRecebidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jConfirmaPagamentoButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                    .addComponent(jLabel5)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jValorRecebidoPanelLayout.setVerticalGroup(
            jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jValorRecebidoPanelLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jValorTotalLabel)
                        .addGap(34, 34, 34)
                        .addComponent(jPagamentoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jValorRecebidoLabel)
                            .addComponent(jValorRecebidoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addGroup(jValorRecebidoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTrocoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTrocoLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jConfirmaPagamentoButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addGap(17, 17, 17))
        );

        jTabbedPane1.addTab("Caixa", jValorRecebidoPanel);

        jEstoqueTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome Produto", "Quantidade no Estoque", "Quantidade Mínima"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane7.setViewportView(jEstoqueTable);
        if (jEstoqueTable.getColumnModel().getColumnCount() > 0) {
            jEstoqueTable.getColumnModel().getColumn(0).setResizable(false);
            jEstoqueTable.getColumnModel().getColumn(1).setResizable(false);
            jEstoqueTable.getColumnModel().getColumn(2).setResizable(false);
        }

        jAddEstoqueButton.setText("Adicionar no Estoque");
        jAddEstoqueButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jAddEstoqueButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jEstoquePanelLayout = new javax.swing.GroupLayout(jEstoquePanel);
        jEstoquePanel.setLayout(jEstoquePanelLayout);
        jEstoquePanelLayout.setHorizontalGroup(
            jEstoquePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEstoquePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jEstoquePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jEstoquePanelLayout.createSequentialGroup()
                        .addComponent(jAddEstoqueButton, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 768, Short.MAX_VALUE))
                .addContainerGap())
        );
        jEstoquePanelLayout.setVerticalGroup(
            jEstoquePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEstoquePanelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jAddEstoqueButton, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(125, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Estoque", jEstoquePanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCancelarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCancelarButtonActionPerformed
        
      
        limpaTela();
        JOptionPane.showMessageDialog(null,"Pedido cancelado !");
        
        
    }//GEN-LAST:event_jCancelarButtonActionPerformed

    private void jConfirmarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jConfirmarButtonActionPerformed
        Pedido umPedido = new Pedido();
        Cliente umCliente = new Cliente();
        
        
        umPedido.setObservacao(jObservacaoTextPane.getText());
        umCliente.setNome(jNomeTextField.getText());
        umPedido.setCliente(umCliente);
        /*
        
            ##Passar objeto pedido para array##
        
        */
        
        limpaTela();
        
        JOptionPane.showMessageDialog(null, "Pedido feito com sucesso !");
    }//GEN-LAST:event_jConfirmarButtonActionPerformed

    private void jAdicionarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jAdicionarButtonActionPerformed
     
        String pedido = jItensList1.getSelectedValue();
        DefaultListModel model = (DefaultListModel) jpedidoList.getModel();
        model.addElement(pedido);
        jpedidoList.setModel(model);
        
    }//GEN-LAST:event_jAdicionarButtonActionPerformed

    private void jPagamentoComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPagamentoComboBoxActionPerformed
       
            
        if(jPagamentoComboBox.getSelectedItem().equals("Dinheiro")){
            
            jTrocoLabel.setVisible(true);
            jTrocoTextField.setVisible(true);
            jValorRecebidoLabel.setVisible(true);
            jValorRecebidoTextField.setVisible(true);
            
        }
        else{
            jTrocoLabel.setVisible(false);
            jTrocoTextField.setVisible(false);
            jValorRecebidoLabel.setVisible(false);
            jValorRecebidoTextField.setVisible(false);
            
        }
        
    }//GEN-LAST:event_jPagamentoComboBoxActionPerformed

    private void jTrocoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTrocoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTrocoTextFieldActionPerformed

    private void jValorRecebidoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jValorRecebidoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jValorRecebidoTextFieldActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jAddEstoqueButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jAddEstoqueButtonActionPerformed
       
        AdicionaEstoque adicionarEstoque = new AdicionaEstoque();
        adicionarEstoque.setVisible(true);
              
        
    }//GEN-LAST:event_jAddEstoqueButtonActionPerformed

    private void jRetirarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRetirarButtonActionPerformed
        String pedido = jpedidoList.getSelectedValue();
        DefaultListModel model = (DefaultListModel) jpedidoList.getModel();
        model.removeElement(pedido);
        jpedidoList.setModel(model);
    }//GEN-LAST:event_jRetirarButtonActionPerformed

    private void jDataTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jDataTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jDataTextFieldActionPerformed

    private void jNomeTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jNomeTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jNomeTextFieldActionPerformed
    
    private void carregarListaPedidos(){
        ArrayList<Pedido> listaPedidos = controlePedidos.getListaPedidos();
        DefaultTableModel model = (DefaultTableModel) jClientesTable.getModel();
        model.setRowCount (0);
        for(Pedido p : listaPedidos){
            model.addRow(new String[]{p.getObservacao()});
        }
        jClientesTable.setModel(model);
    }
    
   
    
    
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel itensLabel;
    private javax.swing.JButton jAddEstoqueButton;
    private javax.swing.JButton jAdicionarButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jCancelarButton;
    private javax.swing.JTable jClientesTable;
    private javax.swing.JButton jConfirmaPagamentoButton;
    private javax.swing.JButton jConfirmarButton;
    private javax.swing.JTextField jDataTextField;
    private javax.swing.JPanel jEstoquePanel;
    private javax.swing.JTable jEstoqueTable;
    private javax.swing.JList<String> jItensList1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jNomeClienteLabel;
    private javax.swing.JTextField jNomeTextField;
    private javax.swing.JLabel jObsLabel;
    private javax.swing.JTextPane jObservacaoTextPane;
    private javax.swing.JComboBox<String> jPagamentoComboBox;
    private javax.swing.JLabel jPedidoLabel;
    private javax.swing.JPanel jPedidosPanel1;
    private javax.swing.JTextPane jPedidosTextPane;
    private javax.swing.JButton jRetirarButton;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel jTrocoLabel;
    private javax.swing.JTextField jTrocoTextField;
    private javax.swing.JLabel jValorRecebidoLabel;
    private javax.swing.JPanel jValorRecebidoPanel;
    private javax.swing.JTextField jValorRecebidoTextField;
    private javax.swing.JLabel jValorTotalLabel;
    private javax.swing.JList<String> jpedidoList;
    // End of variables declaration//GEN-END:variables
}
