
package Controller;

import Model.Pedido;
import java.util.ArrayList;


public class ControlePedidos {
    
    private ArrayList<Pedido> listaPedidos;

    public ControlePedidos(){
        this.listaPedidos = new ArrayList<Pedido>();
        
    }
    
    public ArrayList<Pedido> getListaPedidos() {
        return listaPedidos;
    }
    
   
    public void add(Pedido umPedido) {
        listaPedidos.add(umPedido);
    }
    
    public void excluir (Pedido umPedido){
        listaPedidos.remove(umPedido);
    }
}
