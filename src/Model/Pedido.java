
package Model;

import java.util.ArrayList;
import java.util.Date;


public class Pedido {
    
    private Double precoTotal;
    private String  observacao;
    private Date data;
    private ArrayList<Produto> produtos; 
    private Cliente cliente;
    
        
    
    public Double getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(Double precoTotal) {
        this.precoTotal = precoTotal;
    }

    
    public String getObservacao() {
        return observacao;
    }

    
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    
    public Date getData() {
        return data;
    }

   
    public void Data (Date data) {
        this.data = data;
    }

    
    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

   
    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

  
    public Cliente getCliente() {
        return cliente;
    }

    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

   
    
}
